# Тестовое задание Devops

## Основная часть

1. Написать `develop.Dockerfile` для создания docker образа приложения в develop режиме
2. Написать `docker-compose.yaml` для запуска docker контейнера из предыдущего образа
3. Написать `Dockerfile` для создания docker образа приложения в production режиме

## Дополнительная часть

1. Написать `.gitlab-ci.yaml` с build stage предыдущего образа (`docker build` + `docker push --dry-run`)
2. Добавить lint stage